﻿using Unity.Mathematics;
using UnityEngine;

namespace MatchCards {
    public class Position : MonoBehaviour {
        public float3 Value;
    }
}