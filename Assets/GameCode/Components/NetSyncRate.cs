﻿using Unity.Mathematics;
using UnityEngine;

namespace MatchCards {
    public class NetSyncRate : MonoBehaviour {
        // 64-tick (1 second / 64 ticks per second)
        public float Rate = 0.015625f;
        public float ElapsedTime;
    }
}