﻿using Unity.Mathematics;
using UnityEngine;

namespace MatchCards {
    public class PlayerInput : MonoBehaviour {
        [HideInInspector] public float2 MousePos;
    }
}
