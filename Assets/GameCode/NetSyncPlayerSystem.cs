﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using MatchCards.Network;

namespace MatchCards {
    public class NetSyncPlayerSystem : ComponentSystem {
        public struct Data {
            public int Length;
            public ComponentArray<Position> Position;
            public ComponentArray<NetPosition> NetPosition;
            public ComponentArray<NetSyncRate> NetSyncRate;
            [ReadOnly] public ComponentArray<NetPlayer> NetPlayer;
            [ReadOnly] public ComponentArray<LocalPlayer> LocalPlayer;
        }

        [Inject] private Data data;

        protected override void OnUpdate() {
            if (data.Length == 0) { return; }

            float dt = Time.deltaTime;

            for (int i = 0; i < data.Length; i++) {
                NetSyncRate netSyncRate = data.NetSyncRate[i];
                netSyncRate.ElapsedTime += dt;

                if (netSyncRate.ElapsedTime >= netSyncRate.Rate) {
                    Position position = data.Position[i];
                    NetPosition netPosition = data.NetPosition[i];
                    NetPlayer netPlayer = data.NetPlayer[i];


                    // only update server if the position has changed
                    if (!Mathf.Approximately(netPosition.LastUpdateValue.x, position.Value.x) ||
                        !Mathf.Approximately(netPosition.LastUpdateValue.y, position.Value.y)) {
                        NetClient.singleton.SendPacket(new PktMousePos(netPlayer.NetId, new float2(position.Value.x, position.Value.y)));
                        netPosition.LastUpdateValue = position.Value;
                    }

                    netSyncRate.ElapsedTime -= netSyncRate.Rate;
                }
            }
        }
    }
}