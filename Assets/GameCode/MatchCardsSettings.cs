﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace MatchCards {
    public class MatchCardsSettings : MonoBehaviour {

        public GameObject playerPrefab;

        private void Start() {
            if (FindObjectsOfType<MatchCardsSettings>().Length > 1) {
                Destroy(gameObject);
            } else {
                DontDestroyOnLoad(gameObject);
            }
        }
    }
}