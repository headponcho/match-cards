﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using MatchCards.Network;

namespace MatchCards.UI {

    public class DefaultPort : MonoBehaviour {

        [SerializeField] InputField inputField;

        // Use this for initialization
        void Start() {
            inputField.text = NetServer.DefaultPort.ToString();
        }
    }
}