﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using MatchCards.Network;

public class ConnectMenu : MonoBehaviour {

    [SerializeField] InputField serverPort;

    [SerializeField] InputField clientIp;
    [SerializeField] InputField clientPort;

    public void ServerListen() {
        // TODO sanitize input
        int port = int.Parse(serverPort.text);

        NetServer.singleton.Listen(port);
        ClientConnect("", port);
    }

    public void ClientConnect() {
        // TODO sanitize input
        string ip = clientIp.text;
        int port = int.Parse(clientPort.text);

        ClientConnect(ip, port);
    }

    private void ClientConnect(string ip, int port) {
        if (ip.Equals("")) {
            ip = NetClient.LocalHost;
        }

        NetClient.singleton.Connect(ip, port);
    }
}
