﻿using Unity.Collections;
using Unity.Entities;
using UnityEngine;
using MatchCards.Network;

namespace MatchCards {
    public class PlayerMoveSystem : ComponentSystem {
        public struct Data {
            public int Length;
            public ComponentArray<Position> Position;
            [ReadOnly] public ComponentArray<PlayerInput> Input;
        }

        [Inject] private Data data;

        protected override void OnUpdate() {
            if (data.Length == 0) { return; }

            float dt = Time.deltaTime;

            for (int i = 0; i < data.Length; i++) {
                Position position = data.Position[i];
                PlayerInput input = data.Input[i];

                position.Value.x = input.MousePos.x;
                position.Value.y = input.MousePos.y;
            }
        }
    }
}