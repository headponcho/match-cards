﻿using Unity.Entities;
using Unity.Collections;
using UnityEngine;

namespace MatchCards {
    public class PlayerInputSystem : ComponentSystem {
        struct PlayerData {
            public PlayerInput Input;
            [ReadOnly] public LocalPlayer Local;
        }

        protected override void OnUpdate() {
            float dt = Time.deltaTime;

            foreach (var entity in GetEntities<PlayerData>()) {
                var pi = entity.Input;

                Vector3 mouseScreenPos;
                mouseScreenPos.x = Input.mousePosition.x;
                mouseScreenPos.y = Input.mousePosition.y;
                // assume mouse z-position is at 0.0f
                mouseScreenPos.z = -Camera.main.transform.position.z;

                Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(mouseScreenPos);

                pi.MousePos.x = mouseWorldPos.x;
                pi.MousePos.y = mouseWorldPos.y;
            }
        }
    }
}
