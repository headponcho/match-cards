﻿using System;
using System.Linq;
using System.Collections.Generic;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;
using MatchCards.Network;

namespace MatchCards {
    public sealed class MatchCardsBootstrap {

        public static MatchCardsSettings settings;

        [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
        public static void InitializeWithScene() {
            DisposeAllWorlds();

            var settingsGO = GameObject.Find("Settings");
            settings = settingsGO?.GetComponent<MatchCardsSettings>();
            if (!settings) {
                Debug.LogError("Failed to initialize. No settings found.");
                return;
            }

            CreateMenuWorld();
            Cursor.visible = false;

            Debug.Log("Initializing Packet Manager.");
            PacketManager.InitializePacketTypes();
        }

        public static void DisposeAllWorlds() {
            World.DisposeAllWorlds();
            ScriptBehaviourUpdateOrder.UpdatePlayerLoop();
        }

        public static World CreateServerWorld() {
            World world = CreateWorld("Server");
            GetBehaviourManagerAndLogException(world, typeof(PlayerInputSystem));
            GetBehaviourManagerAndLogException(world, typeof(PlayerMoveSystem));
            GetBehaviourManagerAndLogException(world, typeof(SyncTransformSystem));
            GetBehaviourManagerAndLogException(world, typeof(NetSyncPlayerSystem));

            return world;
        }

        public static World CreateClientWorld() {
            World world = CreateWorld("Client");
            GetBehaviourManagerAndLogException(world, typeof(PlayerInputSystem));
            GetBehaviourManagerAndLogException(world, typeof(PlayerMoveSystem));
            GetBehaviourManagerAndLogException(world, typeof(SyncTransformSystem));
            GetBehaviourManagerAndLogException(world, typeof(NetSyncPlayerSystem));

            return world;
        }

        public static World CreateMenuWorld() {
            World world = CreateWorld("Menu");
            GetBehaviourManagerAndLogException(world, typeof(PlayerInputSystem));
            GetBehaviourManagerAndLogException(world, typeof(PlayerMoveSystem));
            GetBehaviourManagerAndLogException(world, typeof(SyncTransformSystem));

            World.Active = world;

            GameObject player = UnityEngine.Object.Instantiate(MatchCardsBootstrap.settings.playerPrefab, Vector3.zero, Quaternion.identity);
            EntityManager entityManager = World.Active.GetExistingManager<EntityManager>();
            Entity entity = player.GetComponent<GameObjectEntity>().Entity;
            player.AddComponent<LocalPlayer>();
            entityManager.AddComponent(entity, typeof(LocalPlayer));

            ScriptBehaviourUpdateOrder.UpdatePlayerLoop(world);

            return world;
        }

        public static void RegisterWorlds() {
            List<World> worlds = new List<World>();

            if (NetServer.singleton.IsListening) {
                worlds.Add(NetServer.singleton.world);
            }

            if (NetClient.singleton.IsConnected) {
                worlds.Add(NetClient.singleton.world);
            }

            ScriptBehaviourUpdateOrder.UpdatePlayerLoop(worlds.ToArray());
        }

        private static World CreateWorld(string worldName) {
            var world = new World(worldName);
            world.GetOrCreateManager<EntityManager>();

            foreach (var ass in AppDomain.CurrentDomain.GetAssemblies()) {
                var allTypes = ass.GetTypes();

                // Create all ComponentSystem
                var systemTypes = allTypes.Where(t =>
                    t.IsSubclassOf(typeof(ComponentSystemBase)) &&
                    !t.IsAbstract &&
                    !t.ContainsGenericParameters &&
                    t.GetCustomAttributes(typeof(DisableAutoCreationAttribute), true).Length == 0);
                foreach (var type in systemTypes) {
                    if (type.GetCustomAttributes(typeof(ExecuteInEditMode), true).Length == 0)
                        continue;

                    GetBehaviourManagerAndLogException(world, type);
                }
            }

            Debug.Log("Created World: " + worldName);

            return world;
        }

        private static void GetBehaviourManagerAndLogException(World world, Type type) {
            try {
                world.GetOrCreateManager(type);
            } catch (Exception e) {
                Debug.LogException(e);
            }
        }
    }
}
