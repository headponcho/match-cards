﻿using UnityEngine;
using UnityEngine.SceneManagement;
using Unity.Entities;

namespace MatchCards {
    public class WorldTest : MonoBehaviour {

        public GameObject world1Prefab;
        public GameObject world2Prefab;

        World world1;
        World world2;

        // Use this for initialization
        void Start() {
            world1 = MatchCardsBootstrap.CreateServerWorld();
            World.Active = world1;
            Object.Instantiate(world1Prefab, new Vector3(-3.0f, -3.0f, 0.0f), Quaternion.identity);

            world2 = MatchCardsBootstrap.CreateClientWorld();
            World.Active = world2;
            Object.Instantiate(world2Prefab, new Vector3(3.0f, 3.0f, 0.0f), Quaternion.identity);

            World[] worlds = { world1, world2 };

            ScriptBehaviourUpdateOrder.UpdatePlayerLoop(worlds);
        }

        public void World1Active() {
            World.Active = world1;
            var b = world1.BehaviourManagers.GetEnumerator();
            Debug.Log(b.Current);
            while (b.MoveNext()) {
                Debug.Log(b.Current);
            }
        }

        public void World2Active() {
            World.Active = world2;
        }

        public void ChangeScene() {
            SceneManager.LoadScene(1);
        }
    }
}