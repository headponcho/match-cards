﻿using Unity.Mathematics;
using UnityEngine;
using LiteNetLib;
using LiteNetLib.Utils;

namespace MatchCards.Network {
    public class PktAssignNetId : Packet {

        public short netId;
        public float3 initialPos;

        public PktAssignNetId() : this(-1) { }

        public PktAssignNetId(short netId) {
            id = 2;
            clientOnly = true;
            sendOptions = SendOptions.ReliableOrdered;
            statusMessage = "Assigning local NetId.";
            this.netId = netId;
        }

        public override void LoadPacket(NetDataWriter dataWriter) {
            dataWriter.Reset();
            dataWriter.Put(id);

            dataWriter.Put(netId);
        }

        public override void ReadPacket(NetPeer peer, NetDataReader dataReader) {
            netId = dataReader.GetShort();
            NetClient.singleton.SetAssignedNetId(netId);
        }
    }
}