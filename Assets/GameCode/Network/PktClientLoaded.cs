﻿using Unity.Mathematics;
using UnityEngine;
using LiteNetLib;
using LiteNetLib.Utils;

namespace MatchCards.Network {
    public class PktClientLoaded : Packet {

        public int sceneIndex;

        public PktClientLoaded() : this(-1) { }

        public PktClientLoaded(int sceneIndex) {
            id = 0;
            clientOnly = false;
            sendOptions = SendOptions.ReliableUnordered;
            statusMessage = "Client finished loading scene.";
            this.sceneIndex = sceneIndex;
        }

        public override void LoadPacket(NetDataWriter dataWriter) {
            dataWriter.Reset();
            dataWriter.Put(id);

            dataWriter.Put(sceneIndex);
        }

        public override void ReadPacket(NetPeer peer, NetDataReader dataReader) {
            sceneIndex = dataReader.GetShort();

            if (NetServer.singleton.IsListening) {
                NetServer.singleton.OnClientLoaded(peer, sceneIndex);
            }
        }
    }
}