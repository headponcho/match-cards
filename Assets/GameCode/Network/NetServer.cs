﻿using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using Unity.Entities;
using LiteNetLib;
using LiteNetLib.Utils;

namespace MatchCards.Network {

    public class NetServer : NetBase, INetEventListener {

        public static NetServer singleton;

        Dictionary<short, NetPeer> remotePeers = new Dictionary<short, NetPeer>();
        short nextNetId = 0;
        NetDataWriter dataWriter;

        public bool IsListening { get { return netManager != null; } }

        // Use this for initialization
        void Start () {
		    if (FindObjectsOfType<NetServer>().Length > 1) {
                Destroy(gameObject);
            } else {
                DontDestroyOnLoad(gameObject);
                dataWriter = new NetDataWriter();
                singleton = this;
            }
	    }

        // Update is called once per frame
        void Update() {
            if (netManager != null) {
                netManager.PollEvents();
            }
        }

        public void Listen() {
            Listen(NetBase.DefaultPort);
        }

        public void Listen(int port) {
            CloseNetManager();
            netManager = new NetManager(this, NetBase.MaxConnections, NetBase.DefaultConnectKey);
            if (netManager.Start(port)) {
                netManager.UpdateTime = NetBase.DefaultUpdateTime;
                MatchCardsBootstrap.DisposeAllWorlds();
                world = MatchCardsBootstrap.CreateServerWorld();
                World.Active = world;
                MatchCardsBootstrap.RegisterWorlds();

                ServerLog("Started listening on port " + port + ".");
            } else {
                ServerLog("Failed to start listening on port " + port + ".");
                CloseNetManager();
            }
        }

        protected new void CloseNetManager() {
            base.CloseNetManager();
        }

        public void OnPeerConnected(NetPeer peer) {
            ServerLog("Client connected " + peer.EndPoint);
        }

        public void OnClientLoaded(NetPeer peer, int sceneIndex) {
            InstantiatePlayer(peer);
        }

        private void InstantiatePlayer(NetPeer connectedPlayer) {
            ServerLog("Instantiating player " + nextNetId);

            float3 initialPos = new float3(0.0f, 0.0f, 0.0f);
            GameObject player = Object.Instantiate(MatchCardsBootstrap.settings.playerPrefab, initialPos, Quaternion.identity);
            player.GetComponent<NetPlayer>().NetId = nextNetId;
            players.Add(nextNetId, player);

            remotePeers.Add(nextNetId, connectedPlayer);

            PktNewPlayer pktNewPlayer = new PktNewPlayer(nextNetId, initialPos);
            SendToAll(pktNewPlayer);

            PktAssignNetId pktAssignNetId = new PktAssignNetId(nextNetId);
            pktAssignNetId.SendPacket(connectedPlayer, dataWriter);

            SendExistingPlayers(connectedPlayer, nextNetId);

            nextNetId++;
        }

        private void SendExistingPlayers(NetPeer connectedPlayer, short playerNetId) {
            foreach (KeyValuePair<short, GameObject> playerEntry in players) {
                short netId = playerEntry.Key;
                GameObject player = playerEntry.Value;

                if (netId != playerNetId) {
                    PktNewPlayer pktNewPlayer = new PktNewPlayer(netId, player.GetComponent<Position>().Value);
                    pktNewPlayer.SendPacket(connectedPlayer, dataWriter);
                }
            }
        }

        protected override void OnUpdatePlayer(NetPeer peer, short netId, float2 mousePos) {
            PktMousePos pktMousePos = new PktMousePos(netId, mousePos);
            SendToAll(pktMousePos, netId);
        }

        public void RemoveLocalPeer(short netId) {
            remotePeers.Remove(netId);
        }

        private void SendToAll(Packet packet, short excludedPlayerId = -1) {
            foreach (KeyValuePair<short, NetPeer> peer in remotePeers) {
                short netId = peer.Key;

                if (netId != excludedPlayerId) {
                    packet.SendPacket(peer.Value, dataWriter);
                }
            }
        }

        public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo) {
            ServerLog("Client disconnected " + peer.EndPoint + ". Reason: " + disconnectInfo.Reason);
        }

        public void OnNetworkReceive(NetPeer peer, NetDataReader reader) {
            PacketManager.ServerReadPacket(peer, reader);
        }

        public void OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType) {
        }

        public void OnNetworkError(NetEndPoint endPoint, int socketErrorCode) {
        }

        public void OnNetworkLatencyUpdate(NetPeer peer, int latency) {
        }

        public void ServerLog(string logMessage) {
            Debug.Log("[SERVER] " + logMessage);
        }
    }
}