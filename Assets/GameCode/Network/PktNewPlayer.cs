﻿using Unity.Mathematics;
using UnityEngine;
using LiteNetLib;
using LiteNetLib.Utils;

namespace MatchCards.Network {
    public class PktNewPlayer : Packet {

        public short netId;
        public float3 initialPos;

        public PktNewPlayer() : this(-1, 0.0f, 0.0f, 0.0f) { }

        public PktNewPlayer(short netId) : this(netId, 0.0f, 0.0f, 0.0f) { }

        public PktNewPlayer(short netId, float x, float y, float z) : this(netId, new float3(x, y, z)) { }

        public PktNewPlayer(short netId, float3 initialPos) {
            id = 1;
            clientOnly = true;
            sendOptions = SendOptions.ReliableOrdered;
            statusMessage = "Creating new player.";
            this.netId = netId;
            this.initialPos = initialPos;
        }

        public override void LoadPacket(NetDataWriter dataWriter) {
            dataWriter.Reset();
            dataWriter.Put(id);

            dataWriter.Put(netId);
            dataWriter.Put(initialPos.x);
            dataWriter.Put(initialPos.y);
            dataWriter.Put(initialPos.z);
        }

        public override void ReadPacket(NetPeer peer, NetDataReader dataReader) {
            netId = dataReader.GetShort();
            initialPos.x = dataReader.GetFloat();
            initialPos.y = dataReader.GetFloat();
            initialPos.z = dataReader.GetFloat();

            NetClient.singleton.InstantiatePlayer(netId, new Vector3(initialPos.x, initialPos.y, initialPos.z));
        }
    }
}