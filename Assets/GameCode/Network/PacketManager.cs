﻿using System.Collections.Generic;
using UnityEngine;
using LiteNetLib;
using LiteNetLib.Utils;

namespace MatchCards.Network {
    public static class PacketManager {
        static Dictionary<byte, Packet> packets = new Dictionary<byte, Packet>();

        public static void InitializePacketTypes() {
            RegisterPacketType(new PktClientLoaded());
            RegisterPacketType(new PktNewPlayer());
            RegisterPacketType(new PktAssignNetId());
            RegisterPacketType(new PktMousePos());
        }

        private static void RegisterPacketType(Packet packet) {
            byte id = packet.id;
            if (packets.ContainsKey(id)) {
                Debug.LogError("Attempted to register duplicate packet ID: " + id);
                return;
            }

            packets.Add(id, packet);
        }

        public static void ReadPacket(NetPeer peer, NetDataReader dataReader) {
            byte id = dataReader.GetByte();
            Packet packetType = packets[id];
            packetType.ReadPacket(peer, dataReader);
            if (packetType.statusMessage != null) {
                Debug.Log("[CLIENT] " + packetType.statusMessage);
            }
        }

        public static void ServerReadPacket(NetPeer peer, NetDataReader dataReader) {
            byte id = dataReader.GetByte();
            Packet packetType = packets[id];

            if (packetType == null) {
                InvalidPacketWarning(id);
                return;
            }

            if (packetType.clientOnly) {
                InvalidPacketWarning(id);
                return;
            }

            packetType.ReadPacket(peer, dataReader);
            if (packetType.statusMessage != null) {
                Debug.Log("[SERVER] " + packetType.statusMessage);
            }
        }

        private static void InvalidPacketWarning(byte packetId) {
            Debug.LogWarning("Server received an invalid client packet: " + packetId);
        }
    }
}