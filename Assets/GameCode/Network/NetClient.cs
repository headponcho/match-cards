﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Unity.Entities;
using LiteNetLib;
using LiteNetLib.Utils;

namespace MatchCards.Network {

    public class NetClient : NetBase, INetEventListener {

        public static NetClient singleton;

        NetPeer server;
        NetDataWriter dataWriter;

        public bool IsConnected { get { return server != null; } }

        // Use this for initialization
        void Start() {
            if (FindObjectsOfType<NetClient>().Length > 1) {
                Destroy(gameObject);
            } else {
                DontDestroyOnLoad(gameObject);
                dataWriter = new NetDataWriter();
                singleton = this;
            }
        }

        // Update is called once per frame
        void Update() {
            if (netManager != null) {
                netManager.PollEvents();
            }
        }

        public void Connect(string ip, int port) {
            CloseNetManager();
            netManager = new NetManager(this, NetBase.DefaultConnectKey);
            if (netManager.Start()) {
                netManager.UpdateTime = NetBase.DefaultUpdateTime;
                netManager.Connect(ip, port);
            } else {
                ClientLog("Failed to start network manager.");
                CloseNetManager();
            }
        }

        public void SendPacket(Packet packet) {
            if (IsConnected) {
                packet.SendPacket(server, dataWriter);
            }
        }

        public void InstantiatePlayer(short netId, Vector3 position) {
            if (!NetServer.singleton.IsListening) {
                GameObject player = Object.Instantiate(MatchCardsBootstrap.settings.playerPrefab, position, Quaternion.identity);
                player.GetComponent<NetPlayer>().NetId = netId;
                players.Add(netId, player);
            }
        }

        public void SetAssignedNetId(short netId) {
            EntityManager entityManager = World.Active.GetExistingManager<EntityManager>();
            Entity entity = players[netId].GetComponent<GameObjectEntity>().Entity;
            players[netId].AddComponent<LocalPlayer>();
            entityManager.AddComponent(entity, typeof(LocalPlayer));

            // if running a listen server, remove self from remote peer list
            if (NetServer.singleton.IsListening) {
                NetServer.singleton.RemoveLocalPeer(netId);
            }
        }

        public void OnPeerConnected(NetPeer peer) {
            ClientLog("Connected to " + peer.EndPoint);
            server = peer;

            if (!NetServer.singleton.IsListening) {
                MatchCardsBootstrap.DisposeAllWorlds();
                world = MatchCardsBootstrap.CreateClientWorld();
                World.Active = world;
                MatchCardsBootstrap.RegisterWorlds();
            }

            int initialSceneIndex = 1;
            SceneManager.LoadScene(initialSceneIndex);
            PktClientLoaded pktClientLoaded = new PktClientLoaded(initialSceneIndex);
            pktClientLoaded.SendPacket(peer, dataWriter);
        }

        public void OnPeerDisconnected(NetPeer peer, DisconnectInfo disconnectInfo) {
            ClientLog("Disconnected! Reason: " + disconnectInfo.Reason);
            server = null;
        }

        public void OnNetworkReceive(NetPeer peer, NetDataReader reader) {
            PacketManager.ReadPacket(peer, reader);
        }

        public void OnNetworkReceiveUnconnected(NetEndPoint remoteEndPoint, NetDataReader reader, UnconnectedMessageType messageType) {
        }

        public void OnNetworkError(NetEndPoint endPoint, int socketErrorCode) {
        }

        public void OnNetworkLatencyUpdate(NetPeer peer, int latency) {
        }

        public void ClientLog(string logMessage) {
            Debug.Log("[CLIENT] " + logMessage);
        }
    }
}
