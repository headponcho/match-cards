﻿using LiteNetLib;
using LiteNetLib.Utils;

namespace MatchCards.Network {
    abstract public class Packet {
        public byte id;

        // true if this packet should only be received by clients
        public bool clientOnly = false;

        public SendOptions sendOptions = SendOptions.Sequenced;

        public string statusMessage;

        public void SendPacket(NetPeer peer, NetDataWriter dataWriter) {
            LoadPacket(dataWriter);
            peer.Send(dataWriter, sendOptions);
        }

        abstract public void LoadPacket(NetDataWriter dataWriter);

        abstract public void ReadPacket(NetPeer peer, NetDataReader dataReader);
    }
}