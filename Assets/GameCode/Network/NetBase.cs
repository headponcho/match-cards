﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Mathematics;
using LiteNetLib;

namespace MatchCards.Network {

    public class NetBase : MonoBehaviour {

        public const int MaxConnections = 2;
        public const string DefaultConnectKey = "MatchCardsKey";
        public const string LocalHost = "127.0.0.1";
        public const int DefaultPort = 38535;
        public const int DefaultUpdateTime = 15;

        protected NetManager netManager;

        public static Dictionary<short, GameObject> players = new Dictionary<short, GameObject>();
        public World world;

        private void OnDestroy() {
            CloseNetManager();
        }

        protected void CloseNetManager() {
            if (netManager != null) {
                netManager.Stop();
                netManager = null;
                Debug.Log("NetworkManager stopped!");
            }
        }

        public void UpdatePlayer(NetPeer peer, short netId, float2 mousePos) {
            GameObject player = players[netId];
            if (player != null) {
                player.GetComponent<PlayerInput>().MousePos = mousePos;
            } else {
                Debug.LogError("Received update for invalid player: " + netId);
            }
            OnUpdatePlayer(peer, netId, mousePos);
        }

        protected virtual void OnUpdatePlayer(NetPeer peer, short netId, float2 mousePos) {}
    }
}