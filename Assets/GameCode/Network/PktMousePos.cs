﻿using Unity.Mathematics;
using UnityEngine;
using LiteNetLib;
using LiteNetLib.Utils;

namespace MatchCards.Network {
    public class PktMousePos : Packet {

        public short netId;
        public float2 mousePos;

        public PktMousePos() : this(-1, 0.0f, 0.0f) { }

        public PktMousePos(short netId, float x, float y) : this(netId, new float2(x, y)) { }

        public PktMousePos(short netId, float2 mousePos) {
            id = 3;
            clientOnly = false;
            sendOptions = SendOptions.Sequenced;
            this.netId = netId;
            this.mousePos = mousePos;
        }

        public override void LoadPacket(NetDataWriter dataWriter) {
            dataWriter.Reset();
            dataWriter.Put(id);

            dataWriter.Put(netId);
            dataWriter.Put(mousePos.x);
            dataWriter.Put(mousePos.y);
        }

        public override void ReadPacket(NetPeer peer, NetDataReader dataReader) {
            netId = dataReader.GetShort();
            mousePos.x = dataReader.GetFloat();
            mousePos.y = dataReader.GetFloat();

            if (NetServer.singleton.IsListening) {
                NetServer.singleton.UpdatePlayer(peer, netId, mousePos);
            } else if (NetClient.singleton.IsConnected) {
                NetClient.singleton.UpdatePlayer(peer, netId, mousePos);
            }
        }
    }
}