﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using UnityEngine;

namespace MatchCards {
    public class SyncTransformSystem : ComponentSystem {
        public struct Data {
            [ReadOnly] public Position Position;
            public Transform Output;
        }

        protected override void OnUpdate() {
            foreach (Data entity in GetEntities<Data>()) {
                float3 pos = entity.Position.Value;
                entity.Output.position = pos;
            }
        }
    }
}